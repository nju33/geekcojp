#!/bin/sh

set -e

show_help() {
  echo 'sh -s [prepare|reset]';
}

if ! command -v git > /dev/null; then
  echo "マシンに Git がインストールされていません。"
  show_help
  exit 1
fi

command=$1
if test -z "$command"; then
  echo "コマンドは必須です"
  show_help
  exit 1
fi

rootdir="$(git rev-parse --show-toplevel)"

if ! test -e "$rootdir/package.json"; then
  echo "package.jsonがありません"
  exit 1
fi


case "$command" in
  "prepare" ) 
    node -e "
      const pkg = require('./package.json');
      pkg.config.commitizen.path = '../../node_modules/cz-conventional-changelog'

      fs.writeFileSync('${rootdir}/package.json', JSON.stringify(pkg, null, 2));
    "

    git update-index --skip-worktree package.json
    ;;
  "remove" )
    node -e "
      const pkg = require('./package.json');
      pkg.config.commitizen.path = './node_modules/cz-conventional-changelog'

      fs.writeFileSync('${rootdir}/package.json', JSON.stringify(pkg, null, 2));
    "

    git update-index --no-skip-worktree package.json
    ;;
  * )
    echo "知らないコマンド"
    show_help
    exit 1
    ;;
esac
