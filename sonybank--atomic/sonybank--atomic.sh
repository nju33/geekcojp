#!/bin/sh

set -e

show_help() {
  echo 'sh -s [install|reset]';
}

if ! command -v git > /dev/null; then
  echo "マシンに Git がインストールされていません。"
  show_help
  exit 1
fi

command=$1
if test -z "$command"; then
  echo "コマンドは必須です"
  show_help
  exit 1
fi

rootdir="$(git rev-parse --show-toplevel)"

case "$command" in
  "install" ) 
    git fetch

    if test ! -d "${rootdir}/packages"; then
      mkdir packages
    fi

    if test ! -d "${rootdir}/packages/atomic-component"; then
      git worktree add "${rootdir}/packages/atomic-component" atomic-component
    fi

    if test ! -d "${rootdir}/packages/atomic-interactor"; then
      git worktree add "${rootdir}/packages/atomic-interactor" atomic-interactor
    fi

    if test ! -d "${rootdir}/packages/atomic-preset"; then
      git worktree add "${rootdir}/packages/atomic-preset" atomic-preset
    fi

    if test ! -d "${rootdir}/packages/eslint-config"; then
      sh -c "
        cd \"${rootdir}\" \
         && git submodule add -f -b eslint-config git@github.com:geekcojp/sonybank--config.git packages/eslint-config
      "
    fi

    if test ! -d "${rootdir}/packages/tsconfig"; then
      sh -c "
        cd \"${rootdir}\" \
          && git submodule add -f -b tsconfig git@github.com:geekcojp/sonybank--config.git packages/tsconfig
      "
    fi

    if test ! -e "${rootdir}/lerna.json"; then
      curl -sS https://gitlab.com/nju33/geekcojp/raw/master/sonybank--atomic/lerna.json -o "${rootdir}/lerna.json"
      echo lerna.json >> "${rootdir}/.git/info/exclude"
    fi

    if test ! -e "${rootdir}/jest.config.js"; then
      curl -sS https://gitlab.com/nju33/geekcojp/raw/master/sonybank--atomic/jest.config.js -o "${rootdir}/jest.config.js"
      echo jest.config.js >> "${rootdir}/.git/info/exclude"
    fi

    if test ! -e "${rootdir}/tsconfig.json"; then
      curl -sS https://gitlab.com/nju33/geekcojp/raw/master/sonybank--atomic/tsconfig.json -o "${rootdir}/tsconfig.json"
      echo tsconfig.json >> "${rootdir}/.git/info/exclude"
    fi
    
    if test ! -e "${rootdir}/sonybank--atomic.code-workspace"; then
      curl -sS https://gitlab.com/nju33/geekcojp/raw/master/sonybank--atomic/sonybank--atomic.code-workspace -o "${rootdir}/sonybank--atomic.code-workspace"
      echo sonybank--atomic.code-workspace >> "${rootdir}/.git/info/exclude"
    fi

    node -e "
      const pkg = require('${rootdir}/package.json');
      pkg.workspaces = [
        'packages/atomic-component',
        'packages/atomic-interactor',
        'packages/atomic-preset',
        'packages/eslint-config',
        'packages/tsconfig'
      ];
      pkg.devDependencies = {
        '@types/jest': '24.0.18',
        jest: '24.9.0',
        lerna: '3.16.4',
        'ts-jest': '24.0.2',
        typescript: '3.6.2',
      };
      pkg.scripts = {
        dev: 'lerna run dev --scope @sonybank/atomic-component --stream',
      };

      fs.writeFileSync('${rootdir}/package.json', JSON.stringify(pkg, null, 2));
    "
    git update-index --skip-worktree "${rootdir}/package.json"
    echo package.json >> "${rootdir}/.git/info/exclude"
    echo yarn.lock >> "${rootdir}/.git/info/exclude"

    set +x
    sh -c "cd \"${rootdir}\" && yarn"
    set -x

    git reset
    ;;
  "reset" )
    rm -rf "${rootdir}/packages"
    # worktree
    git worktree prune
    set +e
    git branch -D atomiic-component atomic-interactor atomic-preset 2>/dev/null
    set -e

    # submodule
    rm -f "${rootdir}/.gitmodules"
    set +e
    git config --local --get-regex submodule | cut -d' ' -f1 | xargs -I@ -n1 git config --local --unset @ 2>/dev/null
    set -e
    git submodule deinit -f --all
    rm -rf "${rootdir}/.git/modules/packages"

    if test -e "${rootdir}/lerna.json"; then
      rm -f "${rootdir}/lerna.json"

      if command -v gsed > /dev/null; then
        gsed -i "/lerna.json/d" "${rootdir}/.git/info/exclude"
      else
        sed -i "/lerna.json/d" "${rootdir}/.git/info/exclude"
      fi
    fi

    if test -e "${rootdir}/jest.config.js"; then
      rm -f "${rootdir}/jest.config.js"

      if command -v gsed > /dev/null; then
        gsed -i "/jest.config.js/d" "${rootdir}/.git/info/exclude"
      else
        sed -i "/jest.config.js/d" "${rootdir}/.git/info/exclude"
      fi
    fi

    if test -e "${rootdir}/tsconfig.json"; then
      rm -f "${rootdir}/tsconfig.json"

      if command -v gsed > /dev/null; then
        gsed -i "/tsconfig.json/d" "${rootdir}/.git/info/exclude"
      else
        sed -i "/tsconfig.json/d" "${rootdir}/.git/info/exclude"
      fi
    fi

    if test -e "${rootdir}/sonybank--atomic.code-workspace"; then
      rm -f "${rootdir}/sonybank--atomic.code-workspace"

      if command -v gsed > /dev/null; then
        gsed -i "/sonybank--atomic.code-workspace/d" "${rootdir}/.git/info/exclude"
      else
        sed -i "/sonybank--atomic.code-workspace/d" "${rootdir}/.git/info/exclude"
      fi
    fi

    node -e "
      const pkg = require('${rootdir}/package.json');
      delete pkg.workspaces;
      delete pkg.devDependencies;
      delete pkg.scripts;

      fs.writeFileSync('${rootdir}/package.json', JSON.stringify(pkg, null, 2));
    "
    git update-index --no-skip-worktree "${rootdir}/package.json"
    if command -v gsed > /dev/null; then
      gsed -i "/package.json/d" "${rootdir}/.git/info/exclude"
      gsed -i "/yarn.lock/d" "${rootdir}/.git/info/exclude"
    else
      sed -i "/package.json/d" "${rootdir}/.git/info/exclude"
      sed -i "/yarn.lock/d" "${rootdir}/.git/info/exclude"
    fi

    rm -rf "${rootdir}/node_modules"
    ;;
  * )
    echo "知らないコマンド"
    show_help
    exit 1
    ;;
esac
