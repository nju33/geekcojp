#!/bin/sh

set -e

show_help() {
  echo 'sh -s [install|reset]';
}

if ! command -v git > /dev/null; then
  echo "マシンに Git がインストールされていません。"
  show_help
  exit 1
fi

command=$1
if test -z "$command"; then
  echo "コマンドは必須です"
  show_help
  exit 1
fi

case "$command" in
  "install" ) 
    git fetch

    if test ! -d eslint-config; then
      git worktree add eslint-config origin/eslint-config
    fi

    if test ! -d renovate-config; then
      git worktree add renovate-config origin/renovate-config
    fi

    if test ! -d tsconfig; then
      git worktree add tsconfig origin/tsconfig
    fi
    ;;
  "reset" )
    if test -d eslint-config; then
      git worktree remove eslint-config
    fi

    if test -d renovate-config; then
      git worktree remove renovate-config
    fi

    if test -d tsconfig; then
      git worktree remove tsconfig
    fi

    git worktree prune
    set +e
    git branch -d eslint-config tsconfig renovate-config 2>/dev/null
    set -e
    ;;
  * )
    echo "知らないコマンド"
    show_help
    exit 1
    ;;
esac
